import PropTypes from 'prop-types';
import React from 'react';

import {FormattedMessage} from 'react-intl';

import ButtonComponent from '../button/button.jsx';

const hardwareMessage = (
    <FormattedMessage
        defaultMessage="Hardware"
        description="Button to open Arduino connection program"
        id="gui.gui.hardware"
    />
);

const HardwareButtonComponent = ({
                                 onClick,
                                 title,
                                 ...props
                             }) => (
    <span {...props}>
        <ButtonComponent onClick={onClick}>{title}</ButtonComponent>
    </span>
);

HardwareButtonComponent.propTypes = {
    onClick: PropTypes.func.isRequired,
    title: PropTypes.node
};
HardwareButtonComponent.defaultProps = {
    title: hardwareMessage
};
export default HardwareButtonComponent;
