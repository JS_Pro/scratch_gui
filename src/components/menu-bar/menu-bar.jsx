import classNames from 'classnames';
import React from 'react';

import Box from '../box/box.jsx';
import LoadButton from '../../containers/load-button.jsx';
import SaveButton from '../../containers/save-button.jsx';
import HardwareButton from '../../containers/hardware-button.jsx';
import CodeButton from '../../containers/code-button.jsx';
import LanguageSelector from '../../containers/language-selector.jsx';
import LevelSelector from '../../containers/level-selector.jsx';

import styles from './menu-bar.css';
import scratchLogo from './scratch-logo.svg';

class MenuBar extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
	    return (
            <Box
                className={classNames({
                    [styles.menuBar]: true
                })}
            >
                <div className={classNames(styles.logoWrapper, styles.menuItem)}>
                    <img
                        alt="Scratch"
                        className={styles.scratchLogo}
                        draggable={false}
                        src={scratchLogo}
                    />
                </div>
                <SaveButton className={styles.menuItem}/>
                <LoadButton className={styles.menuItem}/>
                <HardwareButton className={styles.menuItem}/>
                <CodeButton vm={this.props.vm} className={styles.menuItem}/>
                <LanguageSelector className={styles.menuItem}/>
            	<LevelSelector vm={this.props.vm} className={styles.menuItem} />
            </Box>
        );
    }
};
export default MenuBar;
