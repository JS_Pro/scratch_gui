import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import GreenFlag from '../green-flag/green-flag.jsx';
import StopAll from '../stop-all/stop-all.jsx';
import Init from '../init/init.jsx'
import TurboMode from '../turbo-mode/turbo-mode.jsx';

import styles from './controls.css';

const Controls = function (props) {
    const {
        active,
        className,
        onGreenFlagClick,
        onStopAllClick,
        onInitClick,
        turbo,
        ...componentProps
    } = props;
    return (
        <div
            className={classNames(styles.controlsContainer, className)}
            {...componentProps}
        >
            <GreenFlag
                active={active}
                onClick={onGreenFlagClick}
            />
            <StopAll
                active={active}
                onClick={onStopAllClick}
            />
            <Init
                active={active}
                onClick={onInitClick}
            />
            {turbo ? (
                <TurboMode />
            ) : null}
        </div>
    );
};

Controls.propTypes = {
    active: PropTypes.bool,
    className: PropTypes.string,
    onGreenFlagClick: PropTypes.func.isRequired,
    onStopAllClick: PropTypes.func.isRequired,
    onInitClick: PropTypes.func.isRequired,
    turbo: PropTypes.bool
};

Controls.defaultProps = {
    active: false,
    turbo: false
};

export default Controls;
