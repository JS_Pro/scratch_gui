import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import initIcon from './icon-init.svg';
import styles from './init.css';

const InitComponent = function (props) {
    const {
        active,
        className,
        onClick,
        title,
        ...componentProps
    } = props;
    return (
        <img
            className={classNames(
                className,
                styles.init,
                {
                    [styles.isActive]: active
                }
            )}
            draggable={false}
            src={initIcon}
            title={title}
            onClick={onClick}
            {...componentProps}
        />
    );
};
InitComponent.propTypes = {
    active: PropTypes.bool,
    className: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    title: PropTypes.string
};
InitComponent.defaultProps = {
    active: false,
    title: 'Init'
};
export default InitComponent;
