import PropTypes from 'prop-types';
import React from 'react';

import Box from '../box/box.jsx';
import levelIcon from './level-icon.svg';
import styles from './level-selector.css';

const levels = {
    basic: {name: 'Basic'},
    intermediate: {name: 'Intermediate'},
    advanced: {name: 'Advanced'}
};

const LevelSelector = ({
    currentLevel,
    onChange,
    ...props
}) => (
    <Box {...props}>
        <div className={styles.group}>
            <img
                className={styles.levelIcon}
                src={levelIcon}
            />
            <select
                aria-label="level selector"
                className={styles.levelSelect}
                value={currentLevel}
                onChange={onChange}
            >
                {Object.keys(levels).map(level => (
                    <option
                        key={level}
                        value={levels[level].name}
                    >
                        {levels[level].name}
                    </option>
                ))}
            </select>
        </div>
    </Box>
);


LevelSelector.propTypes = {
    currentLevel: PropTypes.string,
    onChange: PropTypes.func
};

export default LevelSelector;
