import PropTypes from 'prop-types';
import React from 'react';

import {FormattedMessage} from 'react-intl';

import ButtonComponent from '../button/button.jsx';

const uploadCodeMessage = (
    <FormattedMessage
        defaultMessage="Upload Code"
        description="Button to compile & upload Arduino block code"
        id="gui.gui.uploadCode"
    />
);

const CodeButtonComponent = ({
                                     onClick,
                                     title,
                                     ...props
                                 }) => (
    <span {...props}>
        <ButtonComponent onClick={onClick}>{title}</ButtonComponent>
    </span>
);

CodeButtonComponent.propTypes = {
    onClick: PropTypes.func.isRequired,
    title: PropTypes.node
};
CodeButtonComponent.defaultProps = {
    title: uploadCodeMessage
};
export default CodeButtonComponent;
