import PropTypes from 'prop-types';
import React from 'react';

import ButtonComponent from '../button/button.jsx';
import {ComingSoonTooltip} from '../coming-soon/coming-soon.jsx';

import styles from './load-button.css';

import {FormattedMessage} from 'react-intl';

const loadProjectMessage = (
    <FormattedMessage
        defaultMessage="Load"
        description="Button to load project in the target pane"
        id="gui.gui.loadProject"
    />
);

const LoadButtonComponent = ({
    inputRef,
    onChange,
    onClick,
    title,
    ...props
}) => (
    <span {...props}>
        <ButtonComponent
            // disabled
            onClick={onClick}
        >
            {title}
        </ButtonComponent>
        <input
            // disabled
            accept=".sb2,.sb3"
            className={styles.fileInput}
            ref={inputRef}
            type="file"
            onChange={onChange}
        />
    </span>
);

LoadButtonComponent.propTypes = {
    className: PropTypes.string,
    inputRef: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
    title: PropTypes.node
};
LoadButtonComponent.defaultProps = {
    title: loadProjectMessage
};
export default LoadButtonComponent;
