import {connect} from 'react-redux';
import {updateLevel} from '../reducers/level';
import {updateIntl} from '../reducers/intl.js';
import {updateToolbox} from "../reducers/toolbox.js";
import makeToolboxXML from '../lib/make-toolbox-xml';
import makeToolboxXMLBasic from '../lib/make-toolbox-xml-basic';
import makeToolboxXMLIntermediate from '../lib/make-toolbox-xml-intermediate';
import LevelSelectorComponent from '../components/level-selector/level-selector.jsx';

const mapStateToProps = state => ({
    currentLevel: state.level.level
});

const mapDispatchToProps = dispatch => ({
    onChange: e => {
        e.preventDefault();
        dispatch(updateLevel(e.target.value));

        let toolboxXML;
        if (e.target.value === 'Basic')
            toolboxXML = makeToolboxXMLBasic(false);
        else if (e.target.value === 'Intermediate')
            toolboxXML = makeToolboxXMLIntermediate(false);
        else if (e.target.value === 'Advanced')
            toolboxXML = makeToolboxXML(false);
        dispatch(updateToolbox(toolboxXML));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LevelSelectorComponent);
