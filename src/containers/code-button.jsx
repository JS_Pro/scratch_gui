import bindAll from 'lodash.bindall';
import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import {initCode, isResolvedBlock, resolveBlock, finalizeCode} from '../lib/generate-blocklycode.js';

import CodeButtonComponent from '../components/code-button/code-button.jsx';

class CodeButton extends React.Component {
    constructor (props) {
        super(props);
        bindAll(this, [
            'handleClick'
        ]);
    }

    handleClick () {

        let vmrt = this.props.vm.runtime;
        const numTargets = vmrt.targets.length;

        if(numTargets > 1)
        {
            initCode();

            let target = vmrt.targets[1];
            let blocks = target.blocks;

	        for(let i = 0; i < blocks._scripts.length; i ++) {
            	//  get block id
            	let blockId = blocks._scripts[i];

            	if(blockId !== null && !isResolvedBlock(blockId)) {
            		resolveBlock(blocks, blockId, true, false);
		        }
	        }

            const code = finalizeCode();

            const str_send = `p/8/${code}`;
            fetch("http://localhost:8888/com_arduino", {method: "post",body: str_send,}).then(res => res.json()).then(data => console.log(data));
        }
    }

    render () {
        const {
            ...props
        } = this.props;
        return (
            <CodeButtonComponent
                onClick={this.handleClick}
                {...props}
            />
        );
    }
}

export default CodeButton;
