import React from 'react';
import {initCode, isResolvedBlock, resolveBlock, finalizeCode} from '../lib/generate-blocklycode.js';

class CodeTab extends React.Component {
    constructor (props) {
        super(props);
        this.state = {connected: false};
    }

    render () {

        initCode();
        let code = '';

        let vmrt = this.props.vm.runtime;
        const numTargets = vmrt.targets.length;

        if(numTargets > 1)
        {
            let target = vmrt.targets[1];
            let blocks = target.blocks;

            for (let i = 0; i < blocks._scripts.length; i ++) {
                //  get block id
                let blockId = blocks._scripts[i];

                if (blockId !== null && !isResolvedBlock(blockId)) {
                    resolveBlock(blocks, blockId, true, false);
                }
            }

            code = finalizeCode();
        }

        return (<div>
            <textarea rows = "64" cols = "64">
                {code}
            </textarea>
        </div>);
    }
}

export default CodeTab;
