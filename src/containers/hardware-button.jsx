import bindAll from 'lodash.bindall';
import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';

import HardwareButtonComponent from '../components/hardware-button/hardware-button.jsx';

class HardwareButton extends React.Component {
    constructor (props) {
        super(props);
        bindAll(this, [
            'handleClick'
        ]);
    }
    handleClick () {
        location.href = 'ComArduino://';
    }

    render () {
        const {
            ...props
        } = this.props;
        return (
            <HardwareButtonComponent
                onClick={this.handleClick}
                {...props}
            />
        );
    }
}

export default HardwareButton;
