let KO_Lang = {};
KO_Lang.Blocks = {
    "Motion_Category_Name": "한-Motion",
    "Looks_Category_Name": "한-Looks",
    "Sound_Category_Name": "한-Sound",
    "Events_Category_Name": "한-Events",
    "Control_Category_Name": "한-Control",
    "Sensing_Category_Name": "한-Sensing",
    "Operators_Category_Name": "한-Operators",
    "Variable_Category_Name": "한-Variables",
    "MyBlock_Category_Name": "한-My Blocks",
    "Data_Category_Name": "한-Data",
    "BlocklyDuino_Category_Name": "한-BlocklyDuino",
    "BlocklyFirmware_Category_Name": "한-BlocklyFirmware"
};

let EN_Lang = {};
EN_Lang.Blocks = {
    "Motion_Category_Name": "Motion",
    "Looks_Category_Name": "Looks",
    "Sound_Category_Name": "Sound",
    "Events_Category_Name": "Events",
    "Control_Category_Name": "Control",
    "Sensing_Category_Name": "Sensing",
    "Operators_Category_Name": "Operators",
    "Variable_Category_Name": "Variables",
    "MyBlock_Category_Name": "My Blocks",
    "Data_Category_Name": "Data",
    "BlocklyDuino_Category_Name": "BlocklyDuino",
    "BlocklyFirmware_Category_Name": "BlocklyFirmware"
};

if (typeof exports === "object") {
    exports.KO_Lang = KO_Lang;
    exports.EN_Lang = EN_Lang;
}

