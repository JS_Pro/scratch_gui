let Lang = {};
Lang.category = {
    "name": "en"
};
Lang.type = "en";
Lang.en = "English";
Lang.ko = "한국어";
Lang.Blocks = {
    "Motion_Category_Name": "Motion",
    "Looks_Category_Name": "Looks",
    "Sound_Category_Name": "Sound",
    "Events_Category_Name": "Events",
    "Control_Category_Name": "Control",
    "Sensing_Category_Name": "Sensing",
    "Operators_Category_Name": "Operators",
    "Variable_Category_Name": "Variables",
    "MyBlock_Category_Name": "My Blocks",
    "Data_Category_Name": "Data",
    "BlocklyDuino_Category_Name": "BlocklyDuino",
    "BlocklyFirmware_Category_Name": "BlocklyFirmware"
};

if (typeof exports === "object")
    exports.Lang = Lang;
