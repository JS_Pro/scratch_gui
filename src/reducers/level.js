const UPDATE_LEVEL = 'UPDATE_LEVEL';

const initialState = {
    level: 'Advanced'
};

const reducer = function (state, action) {
    if (typeof state === 'undefined') state = initialState;
    switch (action.type) {
        case UPDATE_LEVEL:
            return Object.assign({}, state, {
                level: action.level
            });
        default:
            return state;
    }
};

const updateLevel = function (level) {
    return {
        type: UPDATE_LEVEL,
        level
    };
};

export {
    reducer as default,
    updateLevel
};
