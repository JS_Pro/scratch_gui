const categorySeparator = '<sep gap="36"/>';

const blockSeparator = '<sep gap="36"/>'; // At default scale, about 28px

let Lang;
const lang_pkg = require(`../lang/ko.js`);

const motion = function (isStage, targetId, locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.Motion_Category_Name + `" colour="#4C97FF" secondaryColour="#3373CC">
        <block type="motion_movesteps">
            <value name="STEPS">
                <shadow type="math_number">
                    <field name="NUM">10</field>
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        <block type="motion_gotoxy">
            <value name="X">
                <shadow id="movex" type="math_number">
                    <field name="NUM">0</field>
                </shadow>
            </value>
            <value name="Y">
                <shadow id="movey" type="math_number">
                    <field name="NUM">0</field>
                </shadow>
            </value>
        </block>
        <block type="motion_goto">
            <value name="TO">
                <shadow type="motion_goto_menu">
                </shadow>
            </value>
        </block>
        <block type="motion_glidesecstoxy">
            <value name="SECS">
                <shadow type="math_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
            <value name="X">
                <shadow id="glidex" type="math_number">
                    <field name="NUM">0</field>
                </shadow>
            </value>
            <value name="Y">
                <shadow id="glidey" type="math_number">
                    <field name="NUM">0</field>
                </shadow>
            </value>
        </block>
        <block type="motion_glideto" id="motion_glideto">
            <value name="SECS">
                <shadow type="math_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
            <value name="TO">
                <shadow type="motion_glideto_menu">
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        <block type="motion_turnright">
            <value name="DEGREES">
                <shadow type="math_number">
                    <field name="NUM">15</field>
                </shadow>
            </value>
        </block>
        <block type="motion_turnleft">
            <value name="DEGREES">
                <shadow type="math_number">
                    <field name="NUM">15</field>
                </shadow>
            </value>
        </block>
        <block type="motion_pointindirection">
            <value name="DIRECTION">
                <shadow type="math_angle">
                    <field name="NUM">90</field>
                </shadow>
            </value>
        </block>
        <block type="motion_pointtowards">
            <value name="TOWARDS">
                <shadow type="motion_pointtowards_menu">
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        <block type="motion_changexby">
            <value name="DX">
                <shadow type="math_number">
                    <field name="NUM">10</field>
                </shadow>
            </value>
        </block>
        <block type="motion_setx">
            <value name="X">
                <shadow id="setx" type="math_number">
                    <field name="NUM">0</field>
                </shadow>
            </value>
        </block>
        <block type="motion_changeyby">
            <value name="DY">
                <shadow type="math_number">
                    <field name="NUM">10</field>
                </shadow>
            </value>
        </block>
        <block type="motion_sety">
            <value name="Y">
                <shadow id="sety" type="math_number">
                    <field name="NUM">0</field>
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        <block type="motion_ifonedgebounce"/>
        ${blockSeparator}
        <block type="motion_setrotationstyle"/>
        ${blockSeparator}
    </category>
    `;
};

const looks = function (isStage, targetId, locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.Looks_Category_Name + `" colour="#9966FF" secondaryColour="#774DCB">
        ${isStage ? '' : `
        <block type="looks_sayforsecs">
            <value name="MESSAGE">
                <shadow type="text">
                    <field name="TEXT">Hello!</field>
                </shadow>
            </value>
            <value name="SECS">
                <shadow type="math_number">
                    <field name="NUM">2</field>
                </shadow>
            </value>
        </block>
        <block type="looks_say">
            <value name="MESSAGE">
                <shadow type="text">
                    <field name="TEXT">Hello!</field>
                </shadow>
            </value>
        </block>
        <block type="looks_thinkforsecs">
            <value name="MESSAGE">
                <shadow type="text">
                    <field name="TEXT">Hmm...</field>
                </shadow>
            </value>
            <value name="SECS">
                <shadow type="math_number">
                    <field name="NUM">2</field>
                </shadow>
            </value>
        </block>
        <block type="looks_think">
            <value name="MESSAGE">
                <shadow type="text">
                    <field name="TEXT">Hmm...</field>
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        `}
        ${isStage ? `
            <block type="looks_switchbackdropto">
                <value name="BACKDROP">
                    <shadow type="looks_backdrops"/>
                </value>
            </block>
            <block type="looks_switchbackdroptoandwait">
                <value name="BACKDROP">
                    <shadow type="looks_backdrops"/>
                </value>
            </block>
            <block type="looks_nextbackdrop"/>
        ` : `
            <block type="looks_switchcostumeto">
                <value name="COSTUME">
                    <shadow type="looks_costume"/>
                </value>
            </block>
            <block type="looks_nextcostume"/>
            <block type="looks_switchbackdropto">
                <value name="BACKDROP">
                    <shadow type="looks_backdrops"/>
                </value>
            </block>
            <block type="looks_nextbackdrop"/>
            ${blockSeparator}
            <block type="looks_changesizeby">
                <value name="CHANGE">
                    <shadow type="math_number">
                        <field name="NUM">10</field>
                    </shadow>
                </value>
            </block>
            <block type="looks_setsizeto">
                <value name="SIZE">
                    <shadow type="math_number">
                        <field name="NUM">100</field>
                    </shadow>
                </value>
            </block>
        `}
        ${blockSeparator}
        <block type="looks_changeeffectby">
            <value name="CHANGE">
                <shadow type="math_number">
                    <field name="NUM">25</field>
                </shadow>
            </value>
        </block>
        <block type="looks_seteffectto">
            <value name="VALUE">
                <shadow type="math_number">
                    <field name="NUM">0</field>
                </shadow>
            </value>
        </block>
        <block type="looks_cleargraphiceffects"/>
        ${blockSeparator}
        ${isStage ? '' : `
            <block type="looks_show"/>
            <block type="looks_hide"/>
        ${blockSeparator}
            <block type="looks_gotofrontback"/>
            <block type="looks_goforwardbackwardlayers">
                <value name="NUM">
                    <shadow type="math_integer">
                        <field name="NUM">1</field>
                    </shadow>
                </value>
            </block>
        `}
        ${isStage ? `
            <block id="backdropnumbername" type="looks_backdropnumbername"/>
        ` : `
            <block id="${targetId}_costumenumbername" type="looks_costumenumbername"/>
            <block id="backdropnumbername" type="looks_backdropnumbername"/>
            <block id="${targetId}_size" type="looks_size"/>
        `}
        ${categorySeparator}
    </category>
    `;
};

const sound = function (locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.Sound_Category_Name + `" colour="#D65CD6" secondaryColour="#BD42BD">
        <block type="sound_play">
            <value name="SOUND_MENU">
                <shadow type="sound_sounds_menu"/>
            </value>
        </block>
        <block type="sound_playuntildone">
            <value name="SOUND_MENU">
                <shadow type="sound_sounds_menu"/>
            </value>
        </block>
        <block type="sound_stopallsounds"/>
        ${blockSeparator}
        <block type="sound_changeeffectby">
            <value name="VALUE">
                <shadow type="math_number">
                    <field name="NUM">10</field>
                </shadow>
            </value>
        </block>
        <block type="sound_seteffectto">
            <value name="VALUE">
                <shadow type="math_number">
                    <field name="NUM">100</field>
                </shadow>
            </value>
        </block>
        <block type="sound_cleareffects"/>
        ${blockSeparator}
        <block type="sound_changevolumeby">
            <value name="VOLUME">
                <shadow type="math_number">
                    <field name="NUM">-10</field>
                </shadow>
            </value>
        </block>
        <block type="sound_setvolumeto">
            <value name="VOLUME">
                <shadow type="math_number">
                    <field name="NUM">100</field>
                </shadow>
            </value>
        </block>
        <block id="volume" type="sound_volume"/>
        ${categorySeparator}
    </category>
    `;
};

const events = function (isStage, locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.Events_Category_Name + `" colour="#FFD500" secondaryColour="#CC9900">
        <block type="event_whenflagclicked"/>
        <block type="event_whenkeypressed">
        </block>
        ${isStage ? `
            <block type="event_whenstageclicked"/>
        ` : `
            <block type="event_whenthisspriteclicked"/>
        `}
        <block type="event_whenbackdropswitchesto">
        </block>
        ${blockSeparator}
        <block type="event_whengreaterthan">
            <value name="VALUE">
                <shadow type="math_number">
                    <field name="NUM">10</field>
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        <block type="event_whenbroadcastreceived">
        </block>
        <block type="event_broadcast">
            <value name="BROADCAST_INPUT">
                <shadow type="event_broadcast_menu"></shadow>
            </value>
        </block>
        <block type="event_broadcastandwait">
            <value name="BROADCAST_INPUT">
              <shadow type="event_broadcast_menu"></shadow>
            </value>
        </block>
        ${categorySeparator}
    </category>
    `;
};

const control = function (isStage, locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.Control_Category_Name + `" colour="#FFAB19" secondaryColour="#CF8B17">
        <block type="control_wait">
            <value name="DURATION">
                <shadow type="math_positive_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        <block type="control_repeat">
            <value name="TIMES">
                <shadow type="math_whole_number">
                    <field name="NUM">10</field>
                </shadow>
            </value>
        </block>
        <block id="forever" type="control_forever"/>
        ${blockSeparator}
        <block type="control_if"/>
        <block type="control_if_else"/>
        <block id="wait_until" type="control_wait_until"/>
        <block id="repeat_until" type="control_repeat_until"/>
        ${blockSeparator}
        <block type="control_stop"/>
        ${blockSeparator}
        ${isStage ? `
            <block type="control_create_clone_of">
                <value name="CLONE_OPTION">
                    <shadow type="control_create_clone_of_menu"/>
                </value>
            </block>
        ` : `
            <block type="control_start_as_clone"/>
            <block type="control_create_clone_of">
                <value name="CLONE_OPTION">
                    <shadow type="control_create_clone_of_menu"/>
                </value>
            </block>
            <block type="control_delete_this_clone"/>
        `}
        ${categorySeparator}
    </category>
    `;
};

const sensing = function (isStage, locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.Sensing_Category_Name + `" colour="#4CBFE6" secondaryColour="#2E8EB8">
        ${isStage ? '' : `
            <block type="sensing_touchingobject">
                <value name="TOUCHINGOBJECTMENU">
                    <shadow type="sensing_touchingobjectmenu"/>
                </value>
            </block>
            <block type="sensing_touchingcolor">
                <value name="COLOR">
                    <shadow type="colour_picker"/>
                </value>
            </block>
            <block type="sensing_coloristouchingcolor">
                <value name="COLOR">
                    <shadow type="colour_picker"/>
                </value>
                <value name="COLOR2">
                    <shadow type="colour_picker"/>
                </value>
            </block>
            <block type="sensing_distanceto">
                <value name="DISTANCETOMENU">
                    <shadow type="sensing_distancetomenu"/>
                </value>
            </block>
            ${blockSeparator}
        `}
        <block id="askandwait" type="sensing_askandwait">
            <value name="QUESTION">
                <shadow type="text">
                    <field name="TEXT">What's your name?</field>
                </shadow>
            </value>
        </block>
        <block id="answer" type="sensing_answer"/>
        ${blockSeparator}
        <block type="sensing_keypressed">
            <value name="KEY_OPTION">
                <shadow type="sensing_keyoptions"/>
            </value>
        </block>
        <block type="sensing_mousedown"/>
        <block type="sensing_mousex"/>
        <block type="sensing_mousey"/>
        ${isStage ? '' : `
            ${blockSeparator}
            '<block type="sensing_setdragmode" id="sensing_setdragmode"></block>'+
            ${blockSeparator}
        `}
        ${blockSeparator}
        <block id="loudness" type="sensing_loudness"/>
        ${blockSeparator}
        <block id="timer" type="sensing_timer"/>
        <block type="sensing_resettimer"/>
        ${blockSeparator}
        <block id="of" type="sensing_of">
            <value name="OBJECT">
                <shadow id="sensing_of_object_menu" type="sensing_of_object_menu"/>
            </value>
        </block>
        ${blockSeparator}
        <block id="current" type="sensing_current"/>
        <block type="sensing_dayssince2000"/>
        ${categorySeparator}
    </category>
    `;
};

const operators = function (locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.Operators_Category_Name + `" colour="#40BF4A" secondaryColour="#389438">
        <block type="operator_add">
            <value name="NUM1">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
            <value name="NUM2">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
        </block>
        <block type="operator_subtract">
            <value name="NUM1">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
            <value name="NUM2">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
        </block>
        <block type="operator_multiply">
            <value name="NUM1">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
            <value name="NUM2">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
        </block>
        <block type="operator_divide">
            <value name="NUM1">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
            <value name="NUM2">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        <block type="operator_random">
            <value name="FROM">
                <shadow type="math_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
            <value name="TO">
                <shadow type="math_number">
                    <field name="NUM">10</field>
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        <block type="operator_lt">
            <value name="OPERAND1">
                <shadow type="text">
                    <field name="TEXT"/>
                </shadow>
            </value>
            <value name="OPERAND2">
                <shadow type="text">
                    <field name="TEXT"/>
                </shadow>
            </value>
        </block>
        <block type="operator_equals">
            <value name="OPERAND1">
                <shadow type="text">
                    <field name="TEXT"/>
                </shadow>
            </value>
            <value name="OPERAND2">
                <shadow type="text">
                    <field name="TEXT"/>
                </shadow>
            </value>
        </block>
        <block type="operator_gt">
            <value name="OPERAND1">
                <shadow type="text">
                    <field name="TEXT"/>
                </shadow>
            </value>
            <value name="OPERAND2">
                <shadow type="text">
                    <field name="TEXT"/>
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        <block type="operator_and"/>
        <block type="operator_or"/>
        <block type="operator_not"/>
        ${blockSeparator}
        <block type="operator_join">
            <value name="STRING1">
                <shadow type="text">
                    <field name="TEXT">hello</field>
                </shadow>
            </value>
            <value name="STRING2">
                <shadow type="text">
                    <field name="TEXT">world</field>
                </shadow>
            </value>
        </block>
        <block type="operator_letter_of">
            <value name="LETTER">
                <shadow type="math_whole_number">
                    <field name="NUM">1</field>
                </shadow>
            </value>
            <value name="STRING">
                <shadow type="text">
                    <field name="TEXT">world</field>
                </shadow>
            </value>
        </block>
        <block type="operator_length">
            <value name="STRING">
                <shadow type="text">
                    <field name="TEXT">world</field>
                </shadow>
            </value>
        </block>
        <block type="operator_contains" id="operator_contains">
          <value name="STRING1">
            <shadow type="text">
              <field name="TEXT">hello</field>
            </shadow>
          </value>
          <value name="STRING2">
            <shadow type="text">
              <field name="TEXT">world</field>
            </shadow>
          </value>
        </block>
        ${blockSeparator}
        <block type="operator_mod">
            <value name="NUM1">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
            <value name="NUM2">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
        </block>
        <block type="operator_round">
            <value name="NUM">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
        </block>
        ${blockSeparator}
        <block type="operator_mathop">
            <value name="NUM">
                <shadow type="math_number">
                    <field name="NUM"/>
                </shadow>
            </value>
        </block>
        ${categorySeparator}
    </category>
    `;
};

const variables = function (isStage, targetId, locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.Variable_Category_Name + `" colour="#FF8C1A" secondaryColour="#DB6E00">
    </category>
    `;
};

const myBlocks = function (isStage, targetId, locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.MyBlock_Category_Name + `" colour="#FF6680" secondaryColour="#FF4D6A">
    </category>
    `;
};

// blocks we added
const blocklyduino = function (locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.BlocklyDuino_Category_Name + `" colour="#CDDC39" secondaryColour="#AFB42B">

        
        <!--Block of getting analog pin value-->
        <block type="blocklyduino_getanalog" id="blocklyduino_getanalog">
            <value name="ANALOG_PIN">
                <shadow type="analog_options">
                </shadow>
            </value>
        </block>
        
        <!--Block of getting digital pin value-->
        <block type="blocklyduino_getdigital" id="blocklyduino_getdigital">
            <value name="DIGITAL_PIN">
                <shadow type="digital_options">
                </shadow>
            </value>
        </block>
        
        <!--Block of setting digital pin value-->
        <block type="blocklyduino_setdigital" id="blocklyduino_setdigital">
            <value name="DIGITAL_PIN">
                <shadow type="digital_options">
                </shadow>
            </value>
            <value name="DIGITAL_VAL">
                <shadow type="digital_val">
                </shadow>
            </value>
        </block>
    </category>
`;
};

const blocklyfirmware = function (locale) {

    // set language with locale
    if (locale === "ko") Lang = lang_pkg.KO_Lang;
    else if(locale === "en") Lang = lang_pkg.EN_Lang;

    return `
    <category name="` + Lang.Blocks.BlocklyFirmware_Category_Name + `" colour="#A0DC39" secondaryColour="#AFB42B">
    <block type="blocklyfirmware_readanalog" id="blocklyfirmware_readanalog">
      <value name="ANALOG_PIN">
        <shadow type="analog_pins">
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_readdigital" id="blocklyfirmware_readdigital">
      <value name="DIGITAL_PIN">
        <shadow type="digital_pins">
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_writeanalog" id="blocklyfirmware_writeanalog">
      <value name="ANALOG_WRITEPIN">
        <shadow type="analog_writepins">
        </shadow>
      </value>
      <value name="ANALOG_VAL">
        <shadow type="math_number">
          <field name="NUM">0</field>
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_writedigital" id="blocklyfirmware_writedigital">
      <value name="DIGITAL_PIN">
        <shadow type="digital_pins">
        </shadow>
      </value>
      <value name="DIGITAL_VAL">
        <shadow type="digital_stats">
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_pulseIn" id="blocklyfirmware_pulseIn">
      <value name="DIGITAL_PIN">
        <shadow type="digital_pins">
        </shadow>
      </value>
      <value name="DIGITAL_VAL">
        <shadow type="digital_stats">
        </shadow>
      </value>
      <value name="TIME_VAL">
        <shadow type="math_number">
          <field name="NUM">1000</field>
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_pinmode" id="blocklyfirmware_pinmode">
      <value name="DIGITAL_PIN">
        <shadow type="digital_pins">
        </shadow>
      </value>
      <value name="PIN_MODE">
        <shadow type="pin_modes">
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_delay" id="blocklyfirmware_delay">
      <value name="TIME_VAL">
        <shadow type="math_number">
          <field name="NUM">1000</field>
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_serialbaudrate" id="blocklyfirmware_serialbaudrate">
      <value name="SERIAL_PIN">
        <shadow type="serial_pins">
        </shadow>
      </value>
      <value name="BAUDRATE_VAL">
        <shadow type="math_number">
          <field name="NUM">9600</field>
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_serialprintln" id="blocklyfirmware_serialprintln">
      <value name="SERIAL_PIN">
        <shadow type="serial_pins">
        </shadow>
      </value>
      <value name="PRINT_VAL">
        <shadow type="text">
          <field name="TEXT"></field>
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_declarevar" id="blocklyfirmware_declarevar">
      <value name="VAR_NAME">
        <shadow type="text">
          <field name="TEXT">item</field>
        </shadow>
      </value>
      <value name="VAR_TYPE">
        <shadow type="var_types">
        </shadow>
      </value>
      <value name="VAR_VAL">
        <shadow type="text">
          <field name="TEXT"></field>
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_dht" id="blocklyfirmware_dht">
      <value name="DHT_SENSOR">
        <shadow type="dht_sensors">
        </shadow>
      </value>
      <value name="DIGITAL_PIN">
        <shadow type="digital_pins">
        </shadow>
      </value>
      <value name="DHT_TYPE">
        <shadow type="dht_types">
        </shadow>
      </value>
    </block>
    <block type="blocklyfirmware_setup" id="blocklyfirmware_setup"></block>
</category>
`;
};

const xmlOpen = '<xml style="display: none">';
const xmlClose = '</xml>';

/**
 * @param {!boolean} isStage - Whether the toolbox is for a stage-type target.
 * @param {?string} targetId - The current editing target
 * @param {string?} categoriesXML - null for default toolbox, or an XML string with <category> elements.
 * @returns {string} - a ScratchBlocks-style XML document for the contents of the toolbox.
 */
const makeToolboxXML = function (isStage, targetId, categoriesXML, locale = 'en') {
    const gap = [categorySeparator];

    const everything = [
        xmlOpen,
        motion(isStage, targetId, locale), gap,
        looks(isStage, targetId, locale), gap,
        sound(locale), gap,
        events(isStage, locale), gap,
        control(isStage, locale), gap,
        sensing(isStage, locale), gap,
        operators(locale), gap,
        variables(isStage, targetId, locale), gap,
        myBlocks(isStage, targetId, locale), gap,
        blocklyduino(locale),
        blocklyfirmware(locale)
    ];

    if (categoriesXML) {
        everything.push(gap, categoriesXML);
    }

    everything.push(xmlClose);
    return everything.join('\n');
};

export default makeToolboxXML;
