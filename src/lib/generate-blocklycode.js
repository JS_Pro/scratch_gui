let includecode;
let declarecode;
let owncode;
let setupcode;
let loopcode;

let resolvedBlocks;

let writeDigitalCount;
let serialPrintCount;

let setuplinedepth = 1;
let looplinedepth = 1;

const tab = '  ';

function getSerialChoice(argVal) {
    if(argVal === 'Serial') return 'Serial';
    if(argVal === 'SoftwareSerial') return 'mySerial';
    if(argVal === 'SoftwareSerial1') return 'mySerial1';
    if(argVal === 'SoftwareSerial2') return 'mySerial2';
    if(argVal === 'SoftwareSerial3') return 'mySerial3';
}

function getDHTSensorChoice(argVal) {
    if(argVal === 'DHT11') return '11';
    if(argVal === 'DHT21') return '21';
    if(argVal === 'DHT22') return '22';
    if(argVal === 'DHT33') return '33';
    if(argVal === 'DHT44') return '44';
}

function getDHTSenseType(argVal) {
    if(argVal === 'getTemperature') return 'temperature';
    if(argVal === 'getHumidity') return 'humidity';
}

function putTabsForSetup()
{
    for(let i = 0; i < setuplinedepth; i ++)
    {
        setupcode += tab;
    }
}

function putTabsForLoop()
{
    for(let i = 0; i < looplinedepth; i ++)
    {
        loopcode += tab;
    }
}

function endBlock(isSetup)
{
    if(isSetup) {
        setuplinedepth --;
        putTabsForSetup();
        setupcode += '}\n';
    } else {
        looplinedepth --;
        putTabsForLoop();
        loopcode += '}\n';
    }
}

function addCode (code, isSetup, beginBlock)
{
    if(isSetup) {
        putTabsForSetup();
        setupcode += code + '\n';

        if (beginBlock === true)
            setuplinedepth ++;
    } else {
        putTabsForLoop();
        loopcode += code + '\n';

        if (beginBlock === true)
            looplinedepth ++;
    }
}

function addSetupCode (code)
{
    putTabsForSetup();
    setupcode += code + '\n';
}

export function resolveBlock (blocks, blockId, isFinal, isSetup) {

    resolvedBlocks.push(blockId);

    const block = blocks.getBlock(blockId);

    if (block.opcode === 'blocklyfirmware_setup') {

        const substack = blocks.getBranch(blockId, 1);
        if(substack)
        {
            resolveBlock(blocks, substack, true, true);
        }
    } else if (block.opcode === 'control_if' || block.opcode === 'control_if_else' ||
        block.opcode === 'control_repeat_until') {

        let conditionInput = null;

        for (const input in block.inputs) {
            // Ignore blocks prefixed with branch prefix.
            if (input.substring(0, 8) !==
                'SUBSTACK') {
                conditionInput = block.inputs[input];
                break;
            }
        }

        let condition = '';
        if(conditionInput !== null) {
            condition = resolveBlock(blocks, conditionInput.block, false, isSetup);
        }

        if(block.opcode === 'control_if' || block.opcode === 'control_if_else') {
            const newcode = 'if ( ' + condition + ' ) {';
            addCode(newcode, isSetup, true);
        } else if(block.opcode === 'control_repeat_until') {
            const newcode = 'while ( !(' + condition + ') ) {';
            addCode(newcode, isSetup, true);
        }

        let substack = blocks.getBranch(blockId, 1);
        if(substack)
        {
            resolveBlock(blocks, substack, true, isSetup);
        }

        endBlock(isSetup);

        //  one more substack for if-else statement
        if(block.opcode === 'control_if_else') {

            addCode('else {', isSetup, true);

            substack = blocks.getBranch(blockId, 2);
            if(substack)
            {
                resolveBlock(blocks, substack, true, isSetup);
            }

            endBlock(isSetup);
        }
    } else {

        if(block.shadow) {
            for (const field in block.fields) {
                if (!block.fields.hasOwnProperty(field)) continue;
                const blockField = block.fields[field];
                return blockField.value;
            }
        }

        let fields = [];

        //  extract inputs
        for (const input in block.inputs) {
            if (!block.inputs.hasOwnProperty(input)) continue;
            const blockInput = block.inputs[input];

            if (blockInput.block || blockInput.shadow) {
                if (blockInput.block) {
                    if(blockInput.name !== 'SUBSTACK') {
                        fields.push(resolveBlock(blocks, blockInput.block, false, isSetup));
                    }
                }
                else if (blockInput.shadow) {
                    fields.push(resolveBlock(blocks, blockInput.shadow, false, isSetup));
                }
            }
        }

        if(block.opcode === 'control_forever') {

            addCode('while (true) {', isSetup, true);

            const substack = blocks.getBranch(blockId, 1);
            if(substack)
            {
                resolveBlock(blocks, substack, true, isSetup);
            }

            endBlock(isSetup);
        }
        else if(block.opcode === 'control_repeat') {
            const newcode = 'for (int i = 1; i <= ' + fields[0] + '; i = i + 1) {';

            addCode(newcode, isSetup, true);

            const substack = blocks.getBranch(blockId, 1);
            if(substack)
            {
                resolveBlock(blocks, substack, true, isSetup);
            }

            endBlock(isSetup);
        }
        else if(block.opcode === 'operator_lt') {
            const newcode = fields[0] + ' < ' + fields[1];
            if (isFinal)
                addCode(newcode, isSetup, false);
            else
                return newcode;
        }
        else if(block.opcode === 'operator_equals') {
            const newcode = fields[0] + ' == ' + fields[1];

            if (isFinal)
                addCode(newcode, isSetup, false);
            else
                return newcode;
        }
        else if(block.opcode === 'operator_gt') {
            const newcode = fields[0] + ' > ' + fields[1];

            if (isFinal)
                addCode(newcode, isSetup, false);
            else
                return newcode;
        }
        else if(block.opcode === 'operator_and') {
            const newcode = fields[0] + ' && ' + fields[1];

            if (isFinal)
                addCode(newcode, isSetup, false);
            else
                return newcode;
        }
        else if(block.opcode === 'operator_or') {
            const newcode = fields[0] + ' || ' + fields[1];

            if (isFinal)
                addCode(newcode, isSetup, false);
            else
                return newcode;
        }
        else if(block.opcode === 'operator_not') {
            const newcode = '!(' + fields[0] + ')';

            if (isFinal)
                addCode(newcode, isSetup, false);
            else
                return newcode;
        }
        else if(block.opcode === 'operator_add') {
            const newcode = fields[0] + ' + ' + fields[1];
            if (isFinal)
                addCode(newcode, isSetup, false);
            else
                return newcode;
        }
        else if(block.opcode === 'operator_subtract') {
            const newcode = fields[0] + ' - ' + fields[1];
            if (isFinal)
                addCode(newcode, isSetup, false);
            else
                return newcode;
        }
        else if(block.opcode === 'operator_multiply') {
            const newcode = fields[0] + ' * ' + fields[1];
            if (isFinal) {
                addCode(newcode, isSetup, false);
            } else {
                return newcode;
            }
        }
        else if(block.opcode === 'operator_divide') {
            const newcode = fields[0] + ' / ' + fields[1];
            if (isFinal)
                addCode(newcode, isSetup, false);
            else
                return newcode;
        }
        else if (block.opcode === 'blocklyfirmware_readanalog') {
            const newcode = 'analogRead(' + fields[0] + ')';
            if (isFinal)
                addCode(newcode + ';', isSetup, false);
            else
                return newcode;
        }
        else if (block.opcode === 'blocklyfirmware_readdigital') {
            const newcode = 'digitalRead(' + fields[0] + ')';
            if (isFinal) {
                addCode(newcode + ';', isSetup, false);
            } else {
                return newcode;
            }
        }
        else if (block.opcode === 'blocklyfirmware_writeanalog') {
            const newcode = 'analogWrite(' + fields[0] + ', ' + fields[1] + ');';
            addCode(newcode, isSetup, false);
        }
        else if (block.opcode === 'blocklyfirmware_writedigital') {
            const value = (fields[1] === '1' ? 'HIGH' : 'LOW');

            if(writeDigitalCount === 0) {
                const newcode = 'pinMode(' + fields[0] + ', OUTPUT);';

                addSetupCode(newcode);
            }

            writeDigitalCount += 1;

            const newcode = 'digitalWrite(' + fields[0] + ', ' + value + ');';

            addCode(newcode, isSetup, false);
        }
        else if (block.opcode === 'blocklyfirmware_pulseIn') {
            const value = (fields[1] === '1' ? 'HIGH' : 'LOW');

            const newcode = 'pulseIn(' + fields[0] + ', ' + value + ', ' + fields[2] + '000)';

            if (isFinal)
                addCode(newcode + ';', isSetup, false);
            else
                return newcode;
        }
        else if (block.opcode === 'blocklyfirmware_pinmode') {
            const newcode = 'pinMode(' + fields[0] + ', ' + fields[1] + ');';

            addCode(newcode, isSetup, false);
        }
        else if (block.opcode === 'blocklyfirmware_delay') {
            const newcode = 'delay(' + fields[0] + ');';

            addCode(newcode, isSetup, false);
        }
        else if (block.opcode === 'blocklyfirmware_serialbaudrate') {
            let serialChoice = getSerialChoice(fields[0]);

            const newcode = serialChoice + '.begin(' + fields[1] + ');';

            addSetupCode(newcode);
        }
        else if (block.opcode === 'blocklyfirmware_serialprintln') {
            let serialChoice = getSerialChoice(fields[0]);

            let newcode = serialChoice + '.begin(9600);';

            if(serialPrintCount === 0) {
                addSetupCode(newcode);
            }

            serialPrintCount ++;

            newcode = serialChoice + '.println("' + fields[1] + '");';

            addCode(newcode, isSetup, false);
        }
        else if (block.opcode === 'blocklyfirmware_declarevar') {
            declarecode += 'volatile ' + fields[1] + ' ' + fields[0] + ';\n';

            if (fields[2].length > 0) {
                const newcode = fields[0] + ' = ' + fields[2] + ';';

                addSetupCode(newcode, false);
            }
        }
        else if (block.opcode === 'blocklyfirmware_dht') {
            const sensor = getDHTSensorChoice(fields[0]);
            const pin = fields[1];
            const sensetype = getDHTSenseType(fields[2]);

            includecode += '#include <dht.h>\n';
            owncode += 'dht myDHT_' + pin + ';\n';
            owncode += 'int dht_' + pin + '_' + fields[2].toLowerCase() + '() {\n';
            owncode += '  int chk = myDHT_' + pin + '.read' + sensor + '(' + pin + ');\n';
            owncode += '  int value = myDHT_' + pin + '.' + sensetype + ';\n  return value;\n}\n';

            const newcode = 'dht_' + pin + '_' + fields[2].toLowerCase() + '()';

            if(isFinal)
                addCode(newcode + ';', isSetup, false);
            else
                return newcode;
        }
    }

    const nextBlockId = blocks.getNextBlock(blockId);

    if(isFinal && nextBlockId) {
        resolveBlock(blocks, nextBlockId, true, isSetup);
    }
}

export function initCode() {
    includecode = '';
    declarecode = '';
    owncode = '';
    setupcode = 'void setup ()\n{\n';
    loopcode = 'void loop()\n{\n';

    writeDigitalCount = 0;
    serialPrintCount = 0;

    resolvedBlocks = [];
}

export function finalizeCode() {
    setupcode += '}';
    loopcode += '}';

    const code = includecode + '\n' +
        declarecode + '\n' +
        owncode + '\n' +
        setupcode + '\n\n' +
        loopcode;

    return code;
}

export function isResolvedBlock(blockId) {
    for(const id in resolvedBlocks) {
        if(blockId === id) return true;
    }
    return false;
}
