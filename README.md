# README #

This is Scratch GUI project.

## Add sprite directly, without showing Sprite Library
File:

	scratch-gui/src/components/target-pane/target-pane.jsx

Origin Code:

	<SpriteSelectorComponent
		...
		onNewSpriteClick={onNewSpriteClick}
		...
	/>

New Code:

	...

	<SpriteSelectorComponent
		...
		onNewSpriteClick={e => {
			let fileSelector = document.createElement('input');
			fileSelector.setAttribute('type', 'file');
			fileSelector.onchange = function () {
				let filename = fileSelector.value.split(/(\\|\/)/g).pop();
				let newitem = {
					"json": {
						"objName": "test",
						"costumes": [
							{
								"baseLayerMD5": filename,
								"rotationCenterX": 22,
								"rotationCenterY": 22
							}
						],
					}
				};
				vm.addSprite2(JSON.stringify(newitem.json));
			};
			fileSelector.click();
		}}
		...
	/>

File:

	scratch-gui/src/lib/storage.js

Origin Code:

	class Storage extends ScratchStorage {

		...
		this.addWebSource(
            [this.AssetType.ImageVector, this.AssetType.ImageBitmap, this.AssetType.Sound],
            asset => `${ASSET_SERVER}/internalapi/asset/${asset.assetId}.${asset.dataFormat}/get/`
        );
	}

New Code:

	class Storage extends ScratchStorage {

		...
		this.addWebSource(
            [this.AssetType.ImageVector, this.AssetType.ImageBitmap, this.AssetType.Sound],
            asset => require(`../../icons/${asset.assetId}.${asset.dataFormat}`)
        );
	}


## Create .exe using electron, electron-packager

### Install electron, electron-packager

    npm install electron --save-dev
    npm install electron-packager -g

### Make build folder

    npm run build

### Create app.js, Update package.json file

Create app.js file like this and put this file to scratch-gui folder:

    const {app, BrowserWindow} = require('electron');
    const path = require('path');
    const url = require('url');

    let window = null;

    // Wait until the app is ready
    app.once('ready', () => {
      // Create a new window
      window = new BrowserWindow({
        // Set the initial width to 800px
        width: 800,
        // Set the initial height to 600px
        height: 600,
        // Set the default background color of the window to match the CSS
        // background color of the page, this prevents any white flickering
        backgroundColor: "#D6D8DC",
        // Don't show the window until it's ready, this prevents any white flickering
        show: false
      });

      // Load a URL in the window to the local index.html path
      window.loadURL(url.format({
        pathname: path.join(__dirname, 'build/index.html'),
        protocol: 'file:',
        slashes: true
      }));

      // Show window when page is ready
      window.once('ready-to-show', () => {
        window.show();
      });
    });

Update package.json file like this:

    "main": "./src/index.js" -> "app.js"
    "start": "webpack-dev-server" -> "electron app.js"

### Create .exe

	electron-packager .

### Note:

	You should install fsevents package forcely in windows like this:

	npm install --force fsevents

## Language Switch Problem 
File:

	scratch-gui/src/lang/ko.js
	
Code:

	let KO_Lang = {};
	KO_Lang.Blocks = {
	    "Motion_Category_Name": "H-Motion",
	    "Looks_Category_Name": "H-Looks",
	    "Sound_Category_Name": "H-Sound",
	    "Events_Category_Name": "H-Events",
	    "Control_Category_Name": "H-Control",
	    "Sensing_Category_Name": "H-Sensing",
	    "Operators_Category_Name": "H-Operators",
	    "Variable_Category_Name": "H-Variables",
	    "MyBlock_Category_Name": "H-My Blocks",
	    "Data_Category_Name": "H-Data",
	    "BlocklyDuino_Category_Name": "H-BlocklyDuino",
	    "BlocklyFirmware_Category_Name": "H-BlocklyFirmware"
	};
		
	let EN_Lang = {};
	EN_Lang.Blocks = {
	    "Motion_Category_Name": "Motion",
	    "Looks_Category_Name": "Looks",
	    "Sound_Category_Name": "Sound",
	    "Events_Category_Name": "Events",
	    "Control_Category_Name": "Control",
	    "Sensing_Category_Name": "Sensing",
	    "Operators_Category_Name": "Operators",
	    "Variable_Category_Name": "Variables",
	    "MyBlock_Category_Name": "My Blocks",
	    "Data_Category_Name": "Data",
	    "BlocklyDuino_Category_Name": "BlocklyDuino",
	    "BlocklyFirmware_Category_Name": "BlocklyFirmware"
	};

	if (typeof exports === "object") {
	    exports.KO_Lang = KO_Lang;
	    exports.EN_Lang = EN_Lang;
	}

File:

	scratch-gui/src/lib/make-toolbox-xml.js
	
Code:

	...

	let Lang;
	const lang_pkg = require(`../lang/ko.js`);

	...

	const motion = function(isStage, targetId, locale) {
	
		// set language with locale
		if (locale === "ko") Lang = lang_pkg.KO_Lang;
		else if (locale === "en") Lang = lang_pkg.EN_Lang;

		...
	}

	const looks = function (isStage, targetId, locale) {
		
		// set language with locale
		if (locale === "ko") Lang = lang_pkg.KO_Lang;
		else if (locale === "en") Lang = lang_pkg.EN_Lang;

		...
	}

	const sound = function (locale) {

		// set language with locale
		if (locale === "ko") Lang = lang_pkg.KO_Lang;
		else if (locale === "en") Lang = lang_pkg.EN_Lang;

		...
	}

	...	// repeat for all category
	
	const blocklyfirmware = function (locale) {

		// set language with locale
		if (locale === "ko") Lang = lang_pkg.KO_Lang;
		else if (locale === "en") Lang = lang_pkg.EN_Lang;

		...
	}

File:

	scratch-gui/package.json
	
Code:

	"react-intl-redux": "0.6.0"
	
	
## Issues

### Javascript Debugging Extension

	JetBrains IDE Support

	
	
### ========== Dev Log for requirement2 ========== ###

### Remove unnecessary Language List Problem
File:

	scratch-gui/node-modules/scratch-l10n/dist/l10n.js
	
Origin Code:

	...
	
	var localeData = _supportedLocales2.default;
	localeData.en.localeData = _en2.default;
	localeData.ar.localeData = _ar2.default;
	localeData.de.localeData = _de2.default;
	localeData.es.localeData = _es2.default;
	localeData.he.localeData = _he2.default;
	localeData.ko.localeData = _ko2.default;

	...
	
	var locales = {
		en: { name: 'English' },
		ar: { name: 'الْعَرَبِيَّة' },
		de: { name: 'Deutsch' },
		es: { name: 'Español' },
		he: { name: 'עִבְרִית' },
		ko: { name: 'Ko' }
	};

	exports.default = locales;
	
New Code:

	...
	
	var localeData = _supportedLocales2.default;
	localeData.en.localeData = _en2.default;
	localeData.ko.localeData = _ko2.default;
	
	...
	
	var locales = {
		en: { name: 'English' },
		ko: { name: 'Ko' }
	};

	exports.default = locales;


### File Upload Icon Call structure (SpriteLibrary)
File:

	src/components/sprite-selector/sprite-selector.jsx

Code:

	const SpriteSelectorComponent = function (props) {

		...

		return (
			<Box
				className={styles.spriteSelector}
				{...componentProps}
			>

				...
				<ActionMenu
					className={styles.addButton}
					img={spriteIcon}
					moreButtons={[
						...
						{
							title: intl.formatMessage(messages.addSpriteFromFile),
							img: fileUploadIcon
						}
						...
					]}
					title={intl.formatMessage(messages.addSpriteFromLibrary)}
					onClick={onNewSpriteClick}
				/>
			</Box>
		);
	};

### Save CSV File when press FileUpload button
File:

	src/components/sprite-selector/sprite-selector.jsx
	
Code:

	title: intl.formatMessage(messages.addSpriteFromFile),
	img: fileUploadIcon
	onClick: function () {
		if (window.FileReader && window.Blob) {
			const myCsv = "Col1,Col2,Col3\nval1,val2,val3";
			const saveLink = document.createElement('a');
			document.body.appendChild(saveLink);
			const data = new Blob([myCsv], {type: 'text/plain'});
			const url = window.URL.createObjectURL(data);
			saveLink.href = url;
			saveLink.download = `test.csv`;
			saveLink.click();
			window.URL.revokeObjectURL(url);
			document.body.removeChild(saveLink);
		}
	}
	
### Read local SVG file and save when press FileUpload button
File:

	src/components/sprite-selector/sprite-selector.jsx

Code:

	onClick: function () {
		if (window.FileReader && window.Blob) {
			// get contents of svg file to text
			let text = '';
			let rawFile = new XMLHttpRequest();
			rawFile.open("GET", require("../../../icons/set-led_blue.svg"), false);
			rawFile.onreadystatechange = function ()
			{
				if(rawFile.readyState === 4)
				{
					if(rawFile.status === 200 || rawFile.status === 0)
					{
						text = rawFile.responseText;
					}
				}
			};
			rawFile.send(null);

			// save svg file in local
			const saveLink = document.createElement('a');
			document.body.appendChild(saveLink);
			const data = new Blob([text], {type: 'text/plain'});
			const url = window.URL.createObjectURL(data);
			saveLink.href = url;
			saveLink.download = 'test.svg';
			saveLink.click();
			window.URL.revokeObjectURL(url);
			document.body.removeChild(saveLink);
		}
	}

### "Save SVG" button in Costumes tab
File:

	scratch-gui/node_modules/scratch-paint/dist/scratch-paint.js

Code:

	_react2.default.createElement(
		'div',
		null,
		_react2.default.createElement(
			'div',
			{
				className: _paintEditor2.default.bitmapButton,
				onClick: function () {
					window.alert('A');
				}
			},

			_react2.default.createElement('img', {
				className: _paintEditor2.default.bitmapButtonIcon,
				draggable: false,
				src: _bitmap2.default
			}),
			_react2.default.createElement(
				'span',
				null,
				props.intl.formatMessage(messages.save)
			)
		)
	)

### ========== Dev Log for requirement1 ========== ##

### "Add Sprite" button call structure.
Source files which is called when click "Add sprite" button :

    scratch_gui/src/reducers/modals.js
	scratch_gui/src/containers/target-pane.jsx
	scratch_gui/src/components/sprite-selector/sprite-selector.jsx
	scratch_gui/src/components/target-pane/target-pane.jsx
	scratch_gui/src/components/sprite-selector-item/sprite-selector-item.jsx
    scratch_gui/src/components/costume-canvas/costume-canvas.jsx


Call structure in sprite-selector.jsx

	const addSpriteMessage = (
		<FormattedMessage
			defaultMessage="Add Sprite"
			description="Button to add a sprite in the target pane"
			id="gui.spriteSelector.addSprite"
		/>
	);

	const SpriteSelectorComponent = function (props) {
		...
		<Box>
			...

			<IconButton
                className={styles.addButton}
                img={spriteIcon}
                title={addSpriteMessage}
                onClick={onNewSpriteClick}
			/>
		</Box>

Call structure in components/target-pane/target-pane.jsx

	const TargetPane = ({

		...
		onNewSpriteClick,

			<SpriteSelectorComponent
				...
				onNewSpriteClick={onNewSpriteClick}

				...
				{spriteLibraryVisible ? (
                    <SpriteLibrary
                        vm={vm}
                        onRequestClose={onRequestCloseSpriteLibrary}
                    />
                ) : null}

Call structure in src/containers/target-pane.jsx

	const mapDispatchToProps = dispatch => ({
		onNewSpriteClick: e => {
			e.preventDefault();
			dispatch(openSpriteLibrary());
		},
		onRequestCloseBackdropLibrary: () => {
			dispatch(closeBackdropLibrary());
		},
		onRequestCloseCostumeLibrary: () => {
			dispatch(closeCostumeLibrary());
		},
		onRequestCloseSoundLibrary: () => {
			dispatch(closeSoundLibrary());
		},
		onRequestCloseSpriteLibrary: () => {
			dispatch(closeSpriteLibrary());
		}
	});

	export default connect(
		mapStateToProps,
		mapDispatchToProps
	)(TargetPane);


Call Structure in modal.js file

    const openSpriteLibrary = function () {
        return openModal(MODAL_SPRITE_LIBRARY);
    };

	const openModal = function (modal) {
		return {
			type: OPEN_MODAL,
			modal: modal
		};
	};

	const OPEN_MODAL = 'scratch-gui/modals/OPEN_MODAL';
	const MODAL_SPRITE_LIBRARY = 'spriteLibrary';

Call Structure in sprite-selector-item.jsx

	...
	{props.costumeURL ? (
            <CostumeCanvas
                className={styles.spriteImage}
                height={10}
                url={""}
                width={32}
            />
	) : null}

	CostumeCanvas represents Costume canvas inside of Sprite canvas.
	url is web image url.


Call Structure in costume-canvas.jsx

	load () {
        // Draw the icon on our canvas.
        const url = this.props.url;
        if (url.indexOf('.svg') > -1) {
            // Vector graphics: need to download with XDR and rasterize.
            // Queue request asynchronously.
            setTimeout(() => {
                xhr.get({
                    useXDR: true,
                    url: url
                }, (err, response, body) => {
                    if (!err) {
                        svgToImage(body, (svgErr, img) => {
                            if (!svgErr) {
                                this.img = img;
                                this.draw();
                            }
                        });
                    }
                });
            }, 0);

        } else {
            // Raster graphics: create Image and draw it.
            const img = new Image();
            img.src = url;
            img.onload = () => {
                this.img = img;
                this.draw();
            };
        }
    }




### Sprite selector item code
Source file:

	scratch_gui/src/components/sprite-selector-item/sprite-selector-item.jsx

Code:

	const SpriteSelectorItem = props => (
		<ContextMenuTrigger
			attributes={{
				className: classNames(props.className, styles.spriteSelectorItem, {
					[styles.isSelected]: props.selected
				}),
				onClick: props.onClick
			}}
			id={`${props.name}-${contextMenuId}`}
		>
			{props.selected ? (
				<CloseButton
					className={styles.deleteButton}
					size={CloseButton.SIZE_SMALL}
					onClick={props.onDeleteButtonClick}
				/>
			) : null }
			{props.costumeURL ? (
				<CostumeCanvas
					className={styles.spriteImage}
					height={32}
					url={props.costumeURL}
					width={32}
				/>
			) : null}
			<div className={styles.spriteName}>{props.name}</div>
			<ContextMenu id={`${props.name}-${contextMenuId++}`}>
				{props.onDuplicateButtonClick ? (
					<MenuItem onClick={props.onDuplicateButtonClick}>
						<FormattedMessage
							defaultMessage="duplicate"
							description="Menu item to duplicate in the right click menu"
							id="gui.spriteSelectorItem.contextMenuDuplicate"
						/>
					</MenuItem>
				) : null}
				<MenuItem onClick={props.onDeleteButtonClick}>
					<FormattedMessage
						defaultMessage="delete"
						description="Menu item to delete in the right click menu"
						id="gui.spriteSelectorItem.contextMenuDelete"
					/>
				</MenuItem>
			</ContextMenu>
		</ContextMenuTrigger>
	);

SpriteSelectorItem is included in src/components/target-pane/target-pane.jsx

### Sprite Library Configuration file

	scratch-gui\src\lib\libraries\sprites.json

### Sprite Resource URL

	https://cdn.assets.scratch.mit.edu/internalapi/asset/ + md5

	ex: https://cdn.assets.scratch.mit.edu/internalapi/asset/25faec47fbd4ed585751942d8051a4df.svg

### Get resource full Url
Source File

	scratch-gui\src\components\library\library.jsx

Source Code

	class LibraryComponent {

		...

		render() {

			...
			const scratchURL = dataItem.md5 ?
                            `https://cdn.assets.scratch.mit.edu/internalapi/asset/${dataItem.md5}/get/` :
                            dataItem.rawURL;
			return (
				<LibraryItem
					description={dataItem.description}
					featured={dataItem.featured}
					iconURL={scratchURL}
					id={index}
					key={`item_${index}`}
					name={dataItem.name}
					onMouseEnter={this.handleMouseEnter}
					onMouseLeave={this.handleMouseLeave}
					onSelect={this.handleSelect}
				/>
			);

		}
		...
	}

	class LibraryItem {

		...
		render () {
        return this.props.featured ? (
            <div
                className={classNames(styles.libraryItem, styles.featuredItem)}
                onClick={this.handleClick}
            >
                <div>
                    <img
                        className={styles.featuredImage}
                        src={this.props.iconURL}
                    />
                </div>
                <div
                    className={styles.featuredText}
                >
                    <span className={styles.libraryItemName}>{this.props.name}</span>
                    <br />
                    <span className={styles.featuredDescription}>{this.props.description}</span>
                </div>
            </div>
        ) : (
            <Box
                className={styles.libraryItem}
                onClick={this.handleClick}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
            >
                {/* Layers of wrapping is to prevent layout thrashing on animation */}
                <Box className={styles.libraryItemImageContainerWrapper}>
                    <Box className={styles.libraryItemImageContainer}>
                        <img
                            className={styles.libraryItemImage}
                            src={this.props.iconURL}
                        />
                    </Box>
                </Box>
                <span className={styles.libraryItemName}>{this.props.name}</span>
            </Box>
        );
    }
	}

### Function which is called when click sprite item in Sprite Library
LibraryComponent:handleSelect(id) is that.

	class LibraryComponent extends React.Component {

		...

		handleSelect (id) {
			this.props.onRequestClose();
			this.props.onItemSelected(this.getFilteredData()[id]);
		}

		...

		render() {

			...

			return (
				<LibraryItem

					...
					onSelect={this.handleSelect}
					...

				/>
			)
		}
	}

### Local Image Path Problem
In the scratch-gui\src\components\library-item\library-item.jsx,

	...

	<Box className={styles.libraryItemImageContainerWrapper}>
                    <Box className={styles.libraryItemImageContainer}>
                        <img
                            className={styles.libraryItemImage}
                            src={this.props.iconURL}
                        />
                    </Box>
                </Box>

Here, change src={this.props.iconURL} to src={require('./arrow.svg')}

	<Box className={styles.libraryItemImageContainerWrapper}>
                    <Box className={styles.libraryItemImageContainer}>
                        <img
                            className={styles.libraryItemImage}
                            src={require('./arrow.svg')}
                        />
                    </Box>
                </Box>

And, Put the arrow.svg file in scratch-gui\src\components\library-item folder.

Then, Local SVG file(arrow.svg) is appeared in Web Application.


### Call structure when select sprite item in Sprite Library
files:

	scratch-gui/src/components/library/library.jsx
	scratch-gui/src/containers/sprite-library.jsx
	webpack:\\\.\node_modules\scratch-vm\dist\node\scratch-vm.js
	scratch-gui/node_modules/scratch-vm/src/serialization/sb2.js
	scratch-gui\node_modules\scratch-vm\dist\node\scratch-vm.js

functions:

	LibraryComponent:handleSelect() -> this.props.onItemSelected(this.getFilteredData()[id]);
	SpriteLibrary:handleItemSelect(item) -> this.props.vm.addSprite2(JSON.stringify(item.json));
	VirtualMachine:addSprite2(json) ->  ...

In the scratch-gui\node_modules\scratch-vm\dist\node\scratch-vm.js,

	var parseScratchObject = function parseScratchObject(object, runtime, topLevel) {

		...

		if (object.hasOwnProperty('costumes')) {
			for (var i = 0; i < object.costumes.length; i++) {
				var costumeSource = object.costumes[i];
				var costume = {
					name: costumeSource.costumeName,
					bitmapResolution: costumeSource.bitmapResolution || 1,
					rotationCenterX: costumeSource.rotationCenterX,
					rotationCenterY: costumeSource.rotationCenterY,
					skinId: null
				};
				costumePromises.push(loadCostume(costumeSource.baseLayerMD5, costume, runtime));
			}
		}

Here, if comment code "costumePromises.push(...)", then sprite does not appear.

### Sprite JSON file sample

	[
		{
			"name": "Ball",
			"type": "sprite",
			"tags": [
				"things",
				"drawing",
				"sports",
				"vector"
			],
			"info": [
				0,
				5,
				1
			],
			"json": {
				"objName": "Ball",
				"sounds": [
					{
						"soundName": "pop",
						"soundID": -1,
						"md5": "83a9787d4cb6f3b7632b4ddfebf74367.wav",
						"sampleCount": 258,
						"rate": 11025,
						"format": ""
					}
				],
				"costumes": [
					{
						"costumeName": "ball-a",
						"baseLayerMD5": "32fec75837e0b8b4a84c32fe65ac6d62.svg",
						"rotationCenterX": 22,
						"rotationCenterY": 22
					},
					{
						"costumeName": "ball-b",
						"baseLayerMD5": "6393dfab227b818ca102aa007e0d73c9.svg",
						"rotationCenterX": 22,
						"rotationCenterY": 22
					}
				],
				"currentCostumeIndex": 0,
				"scratchX": 0,
				"scratchY": 0,
				"scale": 1,
				"direction": 90,
				"rotationStyle": "normal",
				"isDraggable": false,
				"indexInLibrary": 100000,
				"visible": true,
				"spriteInfo": {}
			}
		}
	]


### Source code which add web source files

	scratch-gui\node_modules\scratch-storage\test\integration\download-known-assets.js
	scratch-gui\node_modules\scratch-vm\node_modules\scratch-storage\test\integration\download-known-assets.js

Source code

	test('addWebSource', t => {

		...

		t.doesNotThrow(() => {
			storage.addWebSource(
				[storage.AssetType.ImageVector, storage.AssetType.ImageBitmap, storage.AssetType.Sound],
				asset => `https://cdn.assets.scratch.mit.edu/internalapi/asset/${asset.assetId}.${asset.dataFormat}/get/`
			);
		});
		t.end();

	});


### Source code files when add sprite (2)

	scratch-gui\node_modules\scratch-vm\dist\node\scratch-vm.js
	scratch-gui\node_modules\scratch-storage\dist\web\scratch-storage.js

In the scratch-gui\node_modules\scratch-storage\dist\web\scratch-storage.js:

	value: function load(assetType, assetId, dataFormat) {

		...
		return new Promise(function (fulfill, reject) {
			var tryNextHelper = function tryNextHelper() {
				...
				helper.load(assetType, assetId, dataFormat).then(function (asset) {
					...
				}

			tryNextHelper();
		});

### Add websource in Storage class
File:

	scratch-gui/src/lib/storage.js

Code:

	const PROJECT_SERVER = 'https://cdn.projects.scratch.mit.edu';
	const ASSET_SERVER = 'https://cdn.assets.scratch.mit.edu';

	class Storage extends ScratchStorage {
		constructor () {
			super();

			...

			this.addWebSource(
				[this.AssetType.ImageVector, this.AssetType.ImageBitmap, this.AssetType.Sound],
				asset => `${ASSET_SERVER}/internalapi/asset/${asset.assetId}.${asset.dataFormat}/get/`
			);
		}
	}

Storage class is called in the following file.

	scratch-gui/src/reducers/vm.js

Code:

	const defaultVM = new VM();
	defaultVM.attachStorage(new Storage());


### Show local svg file
File:

	scratch-gui/src/lib/storage.js

Origin Code:

	this.addWebSource(
		[this.AssetType.ImageVector, this.AssetType.ImageBitmap, this.AssetType.Sound],
		asset => `${ASSET_SERVER}/internalapi/asset/${asset.assetId}.${asset.dataFormat}/get/`
	);

New	Code:

	this.addWebSource(
		[this.AssetType.ImageVector, this.AssetType.ImageBitmap, this.AssetType.Sound],
		asset => require('./blue.svg')
	);

Then, local blue.svg sprite appears when click "Add Sprite" button

Any local svg path is possible. Ex,

	this.addWebSource(
		[this.AssetType.ImageVector, this.AssetType.ImageBitmap, this.AssetType.Sound],
		asset => require('F:/icons/set-led_coral.svg')
	);

Library item with local svg:

	<img
		className={styles.libraryItemImage}
		src={require("F:/icons/set-led_coral.svg")}
	/>

### Show local svg file using variable path
File:

	sprites.json

Code:

	"name": "Ball",
	"svg": "blue.svg",
	...

File:

	scratch-gui/src/components/library/library.jsx

Code:

	...
	{this.getFilteredData().map((dataItem, index) => {
		const scratchURL = dataItem.svg;
		return (
			<LibraryItem
				...
				iconURL={scratchURL}
				...
			/>
		);
	})}

File:

	scratch-gui/src/components/library-item/library-item.jsx

Code:

	<img
		className={styles.libraryItemImage}
		src={require("../../icons/" + this.props.iconURL)}
	/>

And, Create scratch-gui/src/icons folder and put svg files in that folder.

### SpriteLibrary vm initialize structure
File:

	scratch-gui/src/components/target-pane/target-pane.jsx

Code:

	{spriteLibraryVisible ? (
		<SpriteLibrary
			vm={vm}
			onRequestClose={onRequestCloseSpriteLibrary}
		/>
	) : null}

### Sprite Library Visibility structure
File:

	scratch-gui/src/containers/target-pane.jsx

Code:

	const mapStateToProps = state => ({

		...

		spriteLibraryVisible: state.modals.spriteLibrary,

		...
	});

File:

	scratch-gui/src/reducers/modals.js

Code:

	...

	const MODAL_SPRITE_LIBRARY = 'spriteLibrary';

Note:

	If spriteLibraryVisible is different from MODAL_SPRITE_LIBRARY, Sprite Library window does not appear.

### VM Initialize structure
File:

	scratch-gui/src/reducers/vm.js

Code:

	...

	const defaultVM = new VM();
	defaultVM.attachStorage(new Storage());
	const initialState = defaultVM;

	...

### TargetPane call structure
File:

	scratch-gui/src/components/gui/gui.jsx

Code:

	const GUIComponent = props => {

		const {
			basePath,
			children,
			enableExtensions,
			vm,
			onExtensionButtonClick,
			onTabSelect,
			tabIndex,
			...componentProps
		} = props;

		...

		return (

			...

			<Box className={styles.targetWrapper}>
				<TargetPane
					vm={vm}
				/>
			</Box>

			...
		)

### GUIComponent call structure
File:

	scratch-gui/src/containers/gui.jsx

Code:

	render () {

		...
		return (

			<GUIComponent
				...
				vm={vm}
			>
			...
		)

	}






























